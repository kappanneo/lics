
inconsistency = concept which applies to natural deducion derivations

Γ = {φ₁, ..., φₙ} is an insconsistent set of formulae if: Γ⊢⟂ is valid

4.

c) Fermat Last Theorem (FLT): xⁿ+yⁿ=zⁿ has no solution for n>2

   x,y,z∈N⁺
   n∈N, n>2

FTL: ∀x.∀y.∀z.∀n.∀x₁.∀y₁.∀z₁.
        L(s(s(zero)),t)∧
        E(s(x),t,x₁)∧
        E(s(y),t,y₁)∧
        E(s(z),t,z₁)∧
        A(x₁,y₁,z₁)

P: ⊤, ∀...¬ψ is consistent (⊤, ∀...¬ψ ⊬ ⟂)
Q: FLT holds (⊨FTL)

P↔Q

P→Q:
by MT:
¬Q→¬P
