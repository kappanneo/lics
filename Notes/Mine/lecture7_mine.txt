# from lecture 7 (predicate logic syntax)

Any two numbers are divisible by their common divisor:
∀x.∀y.(Div(x,gdc(x,y))∧Div(y,gdc(x,y)))


∃x.∀y.(P(x)∧Q(y)) ⊢ ∀y.∃x.(P(x)∧Q(y)):

1  ∃x.∀y(P(x)∧Q(y)          premise
2  ⌜ y₀                     fresh variable
3  |  ⌜ x₀                  fresh variable
4  |  |  ∀y.(P(x₀)∧Q(y)     assumption
5  |  |  P(x₀)∧Q(y₀)        ∀e 4 with y₀
6  |  ⌞  ∃x.(P(x)∧Q(y₀))    ∃i 5
7  ⌞  ∃x.(P(x)∧Q(y₀))       ∃e 1, 3-6
-------------------------------------------
8  ∀y.∃x.(P(x)∧Q(y))        ∀i 2-7
