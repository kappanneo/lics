
 Fixpoint    Chapter 3.7
 Knaster-Tarski Theorem    F : Pow(S) -> Pow(S)  monotone has a least and greatest
 fixpoint
 This holds for -any- set S  not necessarily finite
 But in this course (and in Chapter 3.7)  we present only the case where S is finite

  S, ->, L

 S finite nonempty
 condition N(s) = { s' | s -> s' }  non empty   only used for CTL
 otherwise no other condition   it can be not connected


 Undecidability of predicate logic

 Undecidable: to decide if a purely universal theory is consistent or not

 Two ideas in this proof:  -one is encoding of puzzles (Post problem), machines
 (2-register machines)
                           -second idea illustrated by the next question

 Question 1:  Consider the theory T
 ----------

 Language  succ(x),  zero, one predicate symbol P(x)

 We write succ^2(x) for succ(succ(x)), succ^3(x) for succ(succ(succ(x))), and so on

 axioms     ∀x (P(x) -> P(succ^2(x))  and
            P(zero)

 Let n be a natural number

 (a)  Show that T ⊢ P(succ^n(zero))   if, and only if, n is even

 (b)  Do we have T ⊢ ¬ P(succ(zero))


 (a)  if, and only if    question  look at 2 directions ->   and <-

 <-  let us prove that if n is even we have  T ⊢ P(succ^n(zero))??

 By induction!!     T ⊢ P(succ^{2m}(zero))
 we prove this by induction on m
    m = 0      T ⊢  P(succ^{2m}(zero)) = P(zero)   and P(zero) is an axiom in T
    from m to m+1 easy(?)
    by IH   we have T ⊢ P(succ^{2m}(zero))     (1)
            we have T ⊢ ∀x (P(x) -> P(succ^{2}(x))
            by forall elim T ⊢ P(succ^{2m}(x)x) -> P(succ^{2(m+1)}(x))   (2)
            by MP we get T ⊢ P(succ^{2(m+1)}(x))   from (1) and (2)

 ->  if T ⊢ P(succ^n(zero))   then  n is even??

 One reflex??  Soundness, Completness and Compactness
  
 Trick: to argue semantically

 We assume T ⊢ P(succ^n(zero))
 this assumption is about -provability-

 Use Soundness!!  -->   we have to define/find a suitable model

 axioms     ∀x (P(x) -> P(succ^2(x))  and
            P(zero)

 domain of this model:   set of all natural numbers 0, 1, 2, ...
 P^M   =   subset of even numbers
 zero^M = 0
 s^M : N -> N     s^M(n) = n+1

 This is a model of the theory T:  P(zero) is true in this model and so is
 the other axiom (if n is even then so is n+2)

 Furthermore   zero^M = 0,  s(zero)^M = 1, s(s(zero))^M = 2
               in general (s^n(x))^M = n

 By -soundness-   if we have T ⊢ P(s^n(zero))
 then P(s^n(zero)) is true in the model M we have just defined

 So    (s^n(zero))^M in P^M
        n            in  the subset of even numbers


 (b)  Do we have T |- ¬ P(succ(zero))?

 axioms     ∀x (P(x) -> P(succ^2(x))  and
            P(zero)

 guess: this should not be provable

 how do we prove something is not provable?

 Reflex?? counter-model   what is used is -soundness-    (can be counter intuitive)

 We have to find a model of T which is also a model of ¬¬ P(succ(zero))
 a model of P(succ(zero))

   some creative step??    always try to find a model which is as simple as possible

   model:    nonempty set  N   zero^M = 0,  s^M: n ↦ n+1
             P^M  = subset of -all- natural numbers
             this is a model of T and also a model of  ¬¬ P(succ(zero))


 Question 1': illustrate that it is decidable if a purely universal
 theory for a language -without function symbols- is consistent or not

  - ∀x (P(x) -> Q(x)), ∀x P(x), ¬Q(a)

  - ∀x (P(x) -> ¬M(x)), S(a)∧M(a), ∀z(S(z) -> P(z)) ⊢ ⊥

  same as ∀x (P(x) -> ¬M(x)), S(a) ∧ M(a) ⊢ ∃z (S(z) ∧ ¬ P(z))

 Known as Bernays-Schönfinkel class and this is very important
 in applications of predicate logic


 Question 2:  We say that a relation > on a set D, is well-founded if, and only if, 
 ----------
 we don't have an infinite sequence a1 > a2 > a3 > ...  in D

 Show that there is no formula ψ(R) such that

       M ⊧ψ(R)    <->    R is well-founded

-- reflex: use compactness


 What should you know about fixpoint??

 reflex: a -monotone- function Pow(S) -> Pow(S) has a least and a greatest fixpoint
 given a finite set S

 WHat does monotone means?    X ⊆ Y   ->   F(X) ⊆ F(Y)


 Question 3: Suppose that the function F : Pow(S) -> Pow(S)   S finite
 ----------
 satisfies   X ⊆ Y   ->   F(Y) ⊆ F(X)

 Does F has necessarily a fixpoint?

    Any guess??

 Show that F ∘ F : X ↦ F(F(X))  has a fixpoint

    We show that F ∘ F is monotone
    

 Question 4:  
 ----------

 If A and B are subsets of S we define

   F : Pow(S) -> Pow(S)
       X      ↦  (X ∩ A) ∪ B

 (a) Explain why F has a least fixpoint and a greatest fixpoint

 (b) What is this least fixpoint? What is this greatest fixpoint?


 Question 5:  is the LTL formula  Fp ∧ Fq -> (F(p∧Fq) ∨ F(q∧Fp)) valid?
 ----------

  For this example: we guess that it should be valid

  Argue for the validity: given an arbitrary model M = S, ->, L
  and a path π in this model

   if π ⊧ Fp and π ⊧Fq
      π^m ⊧p  and π^n ⊧ q   for some n and m
      L(π(m),p) = 1    L(π(n),q) = 1   for some n and m
      
   we have to argue  π ⊧(F(p∧Fq) ∨ F(q∧Fp)) 

     we have n <= m   or   m <= n

    if n <= m    π ⊧ F(p ∧ Fq)
    if m <= n    π ⊧ F(q ∧ Fp)    

    same "algorithm" as for predicate logic
    try to understand the meaning of the formula
    from this guess is the answer is yes or no
        yes ---> argue that the formula is valid for all models
        no  ---> find a countermodel

    if you cannot guess try first to prove validity
      if you cannot hopefully this will suggest a countermodel

 Is the LTL formula  (GFp ∧ GFq)  -> F(p ∧ q) valid?

 Is the LTL formula   G(p -> Xq  ∧  q -> Fp) -> GFp   valid?


 Question 6:  Is the following CTL formula valid 
 ----------

   AG(p -> EF q)   ->    EG(p -> AF q)?



 Question 7:  Show that G(p <-> Xp) ->  Gp ∨ G(¬p) is a valid LTL formula
 ----------

 Question 8: Is the LTL formula  G(p -> (p U ¬p)) -> GF(¬p)  valid?
 ----------

 Question 9: give an example of some property of a transition system which can
 ----------
 be expressed in CTL but not in LTL





 Question 10:   let p1, ..., pn be n atomic formulae
 ----------
 assume that n is -odd- and that we have

 (p1 <-> p2)     ->   (p1 ∧ ... ∧ pn)
 (p2 <-> p3)     ->   (p1 ∧ ... ∧ pn)
 ...
 (p(n-1) <-> pn) ->   (p1 ∧ ... ∧ pn)  
 (pn <-> p1)     ->   (p1 ∧ ... ∧ pn)

 E.g. if n = 3 we have

 (p1 <-> p2)     ->   (p1 ∧ p2 ∧ p3)
 (p2 <-> p3)     ->   (p1 ∧ p2 ∧ p3)
 (p3 <-> p1)     ->   (p1 ∧ p2 ∧ p3)

 Show that we should then have   p1 ∧ ... ∧ pn

 Is this also valid if n is even?



