 Compactness Theorem
 -------------------

 Surprising   non constructive uses infinite sets in a non trivial way
 
 For computer science application of compactness: limitation results

 It shows that there are some notions that we want to use in computer science
  that we cannot express in predicate/first-order logic

 This is a motivation for temporal logic

 First-order/Predicate logic

 First-order theories:

  first-order theory: set (maybe infinite) of closed formulae in a given language

 Γ, Δ

 Model of Γ:  we have M ⊧ ψ   for all ψ in Γ
 model of all formulae in Γ

 Γ ⊢ ψ      we can derive/prove the formula ψ from the theory Γ

 What does Γ ⊢ ψ mean?

 There exists a -finite- derivation/proof of ψ using some
 formulae in Γ as premisses


 Soundness    if Γ ⊢ ψ    then M ⊧ψ for all M model of Γ
                               Γ ⊧ ψ

 Completness  if M ⊧ψ for all M model of Γ   then Γ ⊢ ψ

 Both results are non constructive

 Example:   let Γ be the theory   ∀x (P(x) -> Q(x)), ∀x P(x)
            let ψ   be  ∀x Q(x)

   we have Γ ⊢ ψ

   we show this by giving a proof

   1  ∀x (P(x) -> Q(x))
   2  ∀x P(x)
   3  | x0
   4  | P(x0) -> Q(x0)     forall elim 1 x0
   5  | P(x0)              forall elim 2 x0
   6  | Q(x0)              MP 4, 5
   7  ∀x Q(x)              forall intro 3-6

    ⊢     finite derivation    "world of syntax"

  we have Γ ⊧ ψ

    we show this by looking at an arbitrary model M of Γ
    this model can be infinite
    this model has an underlying set D  P^M ⊆ D   Q^M ⊆ D
    if    M is a model of Γ     M ⊧ ∀x (P(x) -> Q(x))
                                P^M ⊆ Q^M
                                M ⊧ ∀x P(x)
                                D = P^M
          we can derive         D = Q^M                      
                                M ⊧ ∀x Q(x)

       ⊧   "infinite reasoning"   "world of semantics"
           we consider arbitrary set, maybe infinite

     the surprising fact is that these two completely different views coincide

     This is the strength of predicate logic, but it implies some weakness...

    A theory is simply a set of (closed) first-order formulae
    This is also called   Context/theory
    A context/theory can be infinite!!
     
 Consistent/satisfiable theory      Γ has a model
 
 Inconsistent/unsatisfiable theory  Γ has no model   semantics
                                    Γ ⊢ ⊥            syntax

 By soundness    if Γ ⊢ ⊥     then  Γ has no model
                       if M ⊧Γ then by soundness M ⊧⊥ contradiction
                       ¬ (M ⊧Γ)
 By completeness if Γ has no model  then   Γ ⊢ ⊥
                       by completeness   since M ⊧Γ -> M ⊧⊥   we have Γ ⊢ ⊥
                       this holds since Γ has no model 

 Key point: in a proof of Γ ⊢ ψ  we only use a finite number of formulae in Γ

 Hence if  Γ ⊢ ψ   there exists X ⊆ Γ -finite-  such that X ⊢ ψ
 In particular if  Γ ⊢ ⊥   there exists X ⊆ Γ -finite-  such that X ⊢ ⊥

 So if Γ is inconsistent there exists a finite subset of Γ which is
 already inconsistent

 This is a form of the compactness Theorem

-- Compactness Theorem: if -all- finite subsets of Γ are consistent,
-- then Γ is consistent, so Γ has a model

 Comment:  we can formulate this result as follows
 -------
    assume that any finite X ⊆ Γ has a model M(X)
    then   Γ has a model

 but the proof is -non constructive-
 We could be able to describe effectively each model M(X)
 but we will not be able to describe effectively a model of Γ
 but only know the -existence- of M ⊧Γ without being able to describe
 M effectively  (we will see examples of this later)

 This is why this result is not so intuitive

  We know the model exists only because it will be contradictory that it does not exist!

 However, what is important is the limitation results about what we can
 express in predicate logic one can prove
 using compactness that show that we cannot express some natural notions
 in first-order logic


 Example: the following theory (not first order in informal language)

 P  =  it is going to rain eventually (for some day in the future)
 P₀ =  not raining today
 P₁ =  not raining tomorrow
 P₂ =  not raining in two days
 ...

 should not be consistent but any finite subsets should be consistent
 e.g. P, P₀, P₂ 

 So any logic of time involving "eventually" cannot satisfy the compactness
 result
 So "eventually" is not something that can be expressed in predicate logic


     This is reminiscent of the "sorites paradox" or
                                "heap of sand paradox" (4th century BC)

     1,000,000 grains is a heap of sand.
     If 1,000,000 grains is a heap then 999,999 grains is a heap.
     So 999,999 grains is a heap.
     If 999,999 grains is a heap then 999,998 grains is a heap.
     So 999,998 grains is a heap.
     ...

     1 grain    it is not a heap of sand
     2 grains   it is not a heap of sand
     ...

     It is a paradox about "vague" predicates



 Theory of graphs:

 Something that cannot be expressed in predicate logic
 This will be a -theorem- that it cannot be expressed (as a consequence
 of the compactness result)

 This is the notion of path   reflexive transitive closure

 The language     R(x,y) binary relation

 A model M   is a (nonempty) set D with a binary relation R^M ⊆ D x D

 So it is a graph

 We want to express that two points in this graph are connected by a path
 crucial notion in computer science

-- Theorem: it is not possible to find a formula ψ(R,x,y) that expresses 
-- that there is a path between x and y

  We assume that there exists such a formula and we deduce a contradiction!

  We extend the language with two constants a and b

  we assume  M ⊧ ψ(R,a,b) 

             if, and only if,  there is a path between a^M and b^M in D

             (a^M,u1) in R^M  ...   (un,b^M) in R^M

 Proof:   assume that there is such a formula ψ(R,x,y)
 Add two constants to the language a and b
 Consider the following infinite theory  Γ

 P  =  ψ(R,a,b)
 P₀ =  a ≠ b
 P₁ =  ¬ R(a,b)                         a not related to b
 P₂ =  ¬ ∃x R(a,x)∧R(x,b)               a not related to b by a path of length 1
 P₃ =  ¬ ∃x ∃y R(a,x)∧R(x,y)∧R(y,b)     a not related to b by a path of length 2
 ...

 Any finite subset X ⊆ Γ is satisfiable

 Indeed in this finite subset X we have for instance P, P₀, P₅, P₉

 We can then form the finite model with a -> x₁ -> ... -> x₉ -> b

 This gives a (finite) model of X

 By compactness Γ is satisfiable

 But in a model M of Γ we have P,P₀,P₁,... that hold
 
  M ⊧ P    there should be a path between a^M and b^M
  M ⊧ P₀    a^M ≠ b^M
  M ⊧ P₁    no path of length 1 between a^M and b^M
  M ⊧ P₂    no path of length 2 between a^M and b^M
  ...

 So we get a contradiction
 This contradicts the hypothesis on ψ that ψ was expression "being connected
 by a path".

 There cannot be any such formula.                □

 This is a negative result: if we want to prove something general about "being
 connected by a path" we cannot use predicate logic.


 Exercise: no formula for being finite
 --------
 
 The language   =

 No formula ψ such that

   M ⊧ ψ  if, and only if, the underlying set of the model M is finite

 So "to be finite" cannot be expressed in predicate logic.

 Proof:
 -----

 The proof will be by contradiction

 We assume that there is such a formula ψ, satisfying for -any- model

    M ⊧ ψ  if, and only if, the underlying set of the model M is finite

 We are going to deduce a contradiction

 For this, we use compactness and we are going a theory that should be
 both consistent and inconsistent (hence the contradiction)

 Γ   P, P₀, P₁, ....

 P = ψ
 P₀ = any suggestion???    ∃x ∃y  x ≠ y    at least 2 elements
 P₁ = ∃x ∃y ∃z ( x ≠ y ∧ y ≠ z ∧ x ≠ z)    at least 3 elements
 P₂ = ...

 Claim: Γ is both consistent and inconsistent

 Γ is inconsistent: it has no model
    if M ⊧ Γ     M ⊧ψ    M should be finite
    but M ⊧ P₀, P₁, ...  M has at least 2, 3, 4, ... elements
    so M should also be infinite   contradiction!!

 Γ is consistent:   compactness!!!
   it is not clear how to build a model of Γ
   but it is clear how to build a model of any finite subset of Γ

   P, P₀, P17, P300     we simply take a finite set with more than 302 elements
    This will be a model of P, P₀, P17, P300

 so we have a contradiction. So there cannot be such a formula ψ.



 Termination/accessibility

  language   relation symbol R

  a model of this language is a graph   (u,v) in R^M

                                        u -> v   computation steps in an abstract way

    we think of the elements of D as stage of computation
     u -> v   we can go from u to v in one step
     we may have several v related to u      non deterministic computation

  one notion that we want to reason about is -termination-

   if a is in D, we cannot have an infinite sequence  a -> a1 -> a2 -> ...

   we are going see this notion in temporal logic   fairness

 Theorem:   termination is not expressible in predicate logic
 -------

 there is no formula ψ(R,x)   such that ψ(R,a) expresses termination for a

 Proof: by compactness  (by contradiction, assume there is such a formula
 and then build an infinite theory which is both inconsistent and
 also consistent using compactness.)

----------------

 -Complete- theories
 -----------------

 Any computer scientist should know about the exact statement of
 Gödel's incompleteness theorem
 should know about the existence of Pressburger arithmetic

       This is not in the book

 "Nothing" to do with completeness theorem

 Completeness theorem: the formal rules of reasoning in predicate logic
 are enough, nothing is missing
 So this is completeness for the derivation rules

 Complete theory: Γ is -complete- if we have Γ ⊢ ψ or Γ ⊢ ¬ ψ  for any closed formula  ψ

 The empty theory is -not- complete

    we don't have     ⊢ ψ
    we don't have     ⊢ ¬ ψ

 where ψ is the formula  ∃x ∃y  x ≠ y

   we don't have ⊢  ∃x ∃y  x ≠ y
   Why?     soundness   if  ⊢ ∃x ∃y  x ≠ y
            it would mean that any model M is a model of ∃x ∃y  x ≠ y
               model with one element!!!
               this is not a model of   ∃x ∃y  x ≠ y
            we have a counter-model

   we don't have ⊢  ¬ (∃x ∃y  x ≠ y)  ≡  ∀x ∀y (x = y)
   what is a counter-model?   any model with at least 2 elements
   

 Pressburger arithmetic: an infinite theory
 ----------------------

    Language    zero, s(x), add(x,y)

    zero ≠ s(x)
    s(x) = s(y) -> x = y
    add(x,0) = x
    add(x,s(y)) = s(add(x,y))

    induction axiom schema, for any formula ψ(x) we add the following

         ψ(zero) ∧ ∀x (ψ(x) -> ψ(s(x)))   ->   ∀x ψ(x)

 Theorem: this theory is complete!!
 -------

  ∀x>8 ∃y ∃z  ( x = 3 y + 5 z )

  not completely obvious
  is it true of not??

   3 y  = (y + y) + y

   we can decide its validity automatically

   automata theory very nice algorithms using automata


 Remark: if Γ is a -complete- theory and M a model of Γ then Γ ⊢ ψ if, and only if, M ⊧ψ
 ------

 Proof?     if Γ ⊢ ψ then M ⊧ψ by soundness
            if M ⊧ ψ   we cannot have Γ ⊢ ¬ ψ (by soundness)
                       so Γ ⊢ ψ    if Γ is complete

 So a complete theory describes "completely" any of its model
 and any two models satisfy the same formulae


 Why is it interesting to have a -complete- first-order theory?

 We have seen last week that there is no algorithm to decide  ⊢ ψ

          no algorithm for:     input ψ           output   tell if ⊢ ψ holds or not


 Theorem:  if Γ is complete there is an algorithm for deciding Γ ⊢ ψ
 -------

 Proof:  enumerate all possible derivations from Γ. Eventually we find
 a proof of Γ ⊢ ψ   or  a proof of Γ ⊢ ¬ ψ

 We can decide any statement about natural numbers involving only addition!!!


     Note that this algorithm is very inefficient. In all examples of complete theories
     I know of, it is possible to find a less trivial algorithm.


   Gödel's incompleteness theorem:   what happens if we add
   multiplication??

   Gödel's incompleteness theorem:   no effective complete theory
   for addition and multiplication


  Peano arithmetic:    PA
  ----------------

    Language    zero, s(x), add(x,y), mult(x,y)

    zero ≠ s(x)
    s(x) = s(y) -> x = y
    add(x,zero) = x
    add(x,s(y)) = s(add(x,y))
    mult(x,zero) = zero
    mult(x,s(y)) = add(x,mult(x,y))

    induction axiom schema, for any formula ψ(x) we add the following

         ψ(zero) ∧ ∀x (ψ(x) -> ψ(s(x)))   ->   ∀x ψ(x)


  Theorem (Gödel):   this theory is -not- complete

  It means that we have some formula ψ   formula about natural numbers
  using addition and multiplication

   PA ⊢ ψ   PA ⊢ ¬ ψ


 Remark: if we consider only multiplication and not addition, we also have a complete
 theory (Skolem)



 Positive fact: there are very interesting first-order theory that are complete
 ex. Pressburger arithmetic, real closed fields, theory of complex numbers

     this is important in practice e.g. robotics
     and important conceptually (e.g. elementary geometry is decidable)

 Negative fact: no effective theory of arithmetic with addition -and- multiplication can be
 complete; this is Gödel's incompleteness Theorem

     it is easier to describe real/complex numbers than to describe natural numbers!



 
 Next time    temporal logic


  "Simpler" than predicate logic    no quantifications!!!
  everything is decidable   like for propositional logic
