# Logic in Computer Science (LiCS)

Book: [Logic in Computer Science](<./Logic in Computer Science.pdf>)

Book exercise solutions: [huthryan_lics2_sol.pdf](./huthryan_lics2_sol.pdf)

### Introduction and course organization

Slides:
 - [0-lics-org.pdf](./Slides/0-lics-org.pdf)

## Part 1 (Ana Bove)

### Recap: logic, sets, relations, functions and induction

Slides:
 - [lecture1.pdf](./Slides/lecture1.pdf) (logic, sets, relations, functions)
 - [lecture2.pdf](./Slides/lecture2.pdf) (mathematical induction, structural induction)

Readings:
 - Section 1.1 (logic)
 - Section 1.4.2 (mathematical induction)

Extra readings:
 - [set-theory.pdf](./Extra/set-theory.pdf)
 - [relations.pdf](./Extra/relations.pdf)
 - [functions.pdf](./Extra/functions.pdf)
 - [induction.pdf](./Extra/induction.pdf)
 - [structuralInduction.pdf](./Extra/StructuralInduction.pdf)

Recommended exercises:
 - [exercises1.pdf](./Exercises/exercises1.pdf)
   - Logic
   - Sets
   - Relations
   - Functions
   - Mathematical and Course-of-values Induction
   - Inductive Sets and Structural Induction

Exercise notes:
 - [notes-ex1.pdf](./Exercises/Notes/notes-ex1.pdf)
   - Mathematical and Course-of-values Induction: 1, 8
   - Inductive Sets and Structural Induction: 1, 3 (a,b,c)

### Natural deduction for propositional logic

Slides:
 - [lecture3.pdf](./Slides/lecture3.pdf)
 - [lecture4.pdf](./Slides/lecture4.pdf)

Readings:
 - Section 1.2 (natural deduction)
 - Section 1.3 (propositional logic sintax)

Extra readings:
 - [natural-deduction.pdf](./Extra/natural-deduction.pdf)

Recommended exercises:
 - All exercises for section 1.2 (section 1.7 in the book)

Exercise notes:
 -  1.2: 1 (e,f,j,m,r) [notes-ex2.png](./Exercises/Notes/notes-ex2.png)
 -  1.2: 3 (a,b,e,h,k,n,p) [notes-ex3.png](./Exercises/Notes/notes-ex3.png)

Assignment:
 - [assig1.pdf](./Assignments/assig1.pdf)

### Semantics of propositional logic, normal forms

Slides:
 - [lecture5.pdf](./Slides/lecture5.pdf) (propositional logic semantics)
 - [lecture6.pdf](./Slides/lecture6.pdf) (normal forms)

Book:
 - Section 1.4 except 1.4.2 (propositional logic semantics)
 - Section 1.5 (normal forms)

Recommended exercises:
 - All exercises for section 1.4 except 7–11 (Section 1.7 in the book)
 - Exercises 1–14 for section 1.5 (Section 1.7 in the book)

Exercise notes:
   - 1.4: 1, 2 (i), 6 (a,b,d), 12 (a,b,d,e), 17 (a) [notes-ex4.pdf](./Exercises/Notes/notes-ex4.pdf)
   - 1.5: 3 (a), 7 (a,c) [notes-ex5.pdf](./Exercises/Notes/notes-ex5.pdf)

Assignment:
 - [assign2.pdf](./Assignments/assig2.pdf)

### Natural deduction for predicate logic

Slides:
 - [lecture7.pdf](./Slides/lecture7.pdf)

Book:
- Sections 2.1-2.3

Recommended exercises:
 - All exercises for section 2.1, 2.2 and 2.3

Exercise notes:
  - 2.1: 1 (a-d); 2.3: 7 (a,b), 9 (a,e) [notes-ex6.pdf](./Exercises/Notes/notes-ex6.pdf)
  - 2.3: 11 (a,b,d) [notes-ex7.pdf](./Exercises/Notes/notes-ex7.pdf)

Assignment:
 - [assig3.pdf](./Assignments/assig3.pdf)


### Semantics of predicate logic

Slides:
 - [lecture8.pdf](./Slides/lecture8.pdf)

Book:
 - Section 2.4

Recommended exercises:
- All exercises for section 2.4 (Section 2.8 in the book)

Exercise notes:
- 2.4: 3, 5 (a,b), 7, 9 (a,b,c,d), 10, 12 (a) [notes-ex8.png](./Exercises/Notes/notes-ex8.png)

Assignment:
- [assig4.pdf](./Assignments/assig4.pdf)

## Part 2 (Thierry Coquand)

### Undecidability of predicate logic and Post correspondence problem

Slides:
 - [lecture9.pdf](./Slides/lecture9.pdf)

Notes:
 - [lecture9.txt](./Notes/lecture9.txt)

Book:
 - Section 2.5

Recommende exercises:
 - Exercise 2.5.1 for section 2.5 (Section 2.8 in the book)

### Expressivity of predicate logic, compactness, incompleteness

Slides:
 - [lecture10.pdf](./Slides/lecture10.pdf) (fixpoint theorems and SAT solving) [GUEST]
 - [lecture11.pdf](./Slides/lecture11.pdf) (compactness theorem and Gödel's incompleteness theorem)

Notes:
 - [lecture10.txt](./Notes/lecture10.txt) (fixpoint theorems and SAT solving)
 - [lecture11.txt](./Notes/lecture11.txt) (compactness theorem and Gödel's incompletness theorem)

Book:
 - Section 2.6

### Temporal Logic: LTL, CTL

Slides:
 - [lecture12.pdf](./Slides/lecture12.pdf) (Linear Temporal Logic)

Notes:
 - [lecture12.txt](./Notes/lecture12.txt) (Linear Temporal Logic)
 - [lecture13.txt](./Notes/lecture13.txt) (LTL and CTL)
 - [lecture14.txt](./Notes/lecture14.txt) (model checking algorithm for CTL)
 - [lecture15.txt](.Notes/lecture15.txt) (model checking)

Extra Slides:
 - [tl.pdf](./Extra/Slides/tl.pdf) (temporal logics)
 - [WA-GuestLICS.pdf](./Extra/Slides/WA-GuestLICS.pdf) (logic-based software verification)

Book:
 - Sections 3.1-3.5

Recommended exercises:
 - Exercise 3.2.2 and all exercises for section 3.4 (Section 3.8 in the book)

### Algorithms, fix-point characterization

Book:
 - Sections 3.6-3.7

Recommended exercises:
 - All exercises for section 3.7 (Section 3.8 in the book)