
Problem 1

Give a proof in natural deduction of the following sequent,
where f and g are unary function symbols:

   ∀x.∀y.(x=g(y)→f(x)=y) ⊢ ∀x.f(g(x))=x

1  ∀x.∀y.(x=g(y)→f(x)=y)           premise
2  ⌜ x₀                          fresh var
3  |  ∀y.(g(x₀)=g(y)→f(g(x₀))=y)    ∀x.e 1 : g(x₀)
4  |  g(x₀)=g(x₀)→f(g(x₀))=x₀       ∀y.e 3 : x₀
5  |  g(x₀)=g(x₀)                       =i : g(x₀)
6  ⌞  f(g(x₀))=x₀                   →e 4,5
------------------------------------------
7  ∀x.f(g(x))=x                     ∀i 2-6

---------------------------------------------------------------------------

Problem 2

Let P be a binary predicate symbol and let Φ be the formula:

∀x.∃y.∃z.(P(x,y)∧P(y,z)∧∀w.(P(w,x)→P(w,z)))

Do the following models satisfy Φ?

a) M₀ consists of the set of natural numbers N={0,1,2, . . .} with:

   P^M₀:={(m, n)|m < n and m,n∈N}

b) M₁ consists of the set of natural numbers N with:

   P^M₁:={(m, m+2)|m∈N}

---------------------------------------------------------------------------

Problem 3

Argue for each of the sequents below whether they are valid or not,
i.e. either give a proof in natural deduction or a counter-model.
Here, R is a binary, S and T are unary predicate symbols.

a) ∀x.(S(x)→T(x)) ⊢ ∀x.(S(x)∨T(x))

   x∈N
   S(x): x is grater than 3
   T(x): x is not zero


   ⟦∀x.(S(x)→T(x))⟧v = T
   ⟦∀x.(S(x)∨T(x))⟧v = F

b) ∀x.∃y.(S(x)→R(x, y)) ⊢ ∃y.∀x.(S(x)→R(x, y))

c) ∀x.∀y.(R(x,y)→R(y,x)), ∀x.∀y.∀z.(R(x,y)∧R(y,z)→R(x,z)) ⊢ ∀x. R(x,x)

---------------------------------------------------------------------------

Problem 4

Argue semantically that the formula:

   ∃x.∀y.(R(x,y)↔¬∃z(R(y,z)∧R(z,y)))

is not satisfiable.

---------------------------------------------------------------------------

Problem 5

Let a language be given by one unary predicatesymbol Q,
one unary function symbol h and a constant c.
Argue semantically that the following semantical entailments hold:

a) ⊨ ∃x(Q(x)∨¬Q(h(x)))

b) ∀x.(Q(x)→x=c) ⊨ ∀x.∀y.(Q(x)∧Q(y)→x=y)
