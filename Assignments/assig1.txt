Problem 1

1)
p:= "do the crime"
q:= "you can make the time"

¬q → ¬p

2)
m:= Miranda likes pets
h:= Haskel likes pets
k:= Kleen likes pets

(m∨h∨k) ∧ ¬(m∧h∧k)

Problem 2

1) (p∧q)∨(p∧r)⊢p∧(q∨r)

1 (p∧q)∨(p∧r)  premise
2  ⌜   p∧q    assumption
3  |    p       ∧e₁ 2
4  |    q       ∧e₂ 2
5  |   q∨r     ∨i₁ 4
6  ⌞  p∧(q∨r)  ∧i 3,5
7  ⌜   p∧r    assumption
8  |    p       ∧e₁ 7
9  |    r       ∧e₂ 7
10 |   q∨r     ∨i₂ 9
11 ⌞  p∧(q∨r)  ∧i 8,10
---------------------------
12   p∧(q∨r)  ∨e 2-6,7-11

2) p∨q⊢(p→q)→q

1  p∨q         premise
2  ⌜  p→q       assumption
3  |  ⌜  p      assumption
4  |  ⌞  q      →e 2,3
5  |  [  q      assumption
6  ⌞  q         ∨e 1,3-4,5
--------------------------
7  (p→q)→q      →i 2-6

3) p→¬p,¬p→p⊢⟂

1  p→¬p       premise
2  ¬p→p       premise
3  ⌜  p       assumption
4  |  ¬p      →e 1,3
5  ⌞  ⟂       ¬e 4,3
6  ¬p         ¬i 3-5
7  p          →e 2,6
------------------------
8  ⟂          ¬e 6,7

4) ¬(p∧q)⊢¬p∨¬q

1  ¬(p∧q)          premise
2  p∨¬p            LEM
3  ⌜  p             assumption
4  |  ⌜  q          assumption
5  |  |  p∧q       ∧i 3,4
6  |  ⌞  ⟂          ¬e 1,5
7  |  ¬q            ¬i 4-6
8  ⌞  ¬p∨¬q        ∨i₂ 7
9  ⌜  ¬p            assumption
10 ⌞  ¬p∨¬q         ∨i₁ 9
------------------------------
11 ¬p∨¬q         ∨e 2,3-8,9-10

Problem 3

1) ⊢((p→q)→p)→p (Peirce's Law)

1  p∨¬p                 LEM
2  ⌜  p                 assumption
3  |  ⌜ (p→q)→p         assumption
4  |  ⌞  p              copy 2
5  ⌞  ((p→q)→p)→p       →i 3-4
6  ⌜  ¬p                assumption
7  |  ⌜  p              assumption
8  |  |  ⟂              ¬e 6,7
9  |  ⌞  q              ⟂e 8
10 |  p→q               →i 7-9
11 |  ⌜ (p→q)→p         assumption
12 |  ⌞  p              →e 11,10
13 ⌞  ((p→q)→p)→p       →i 11,12
----------------------------------
14 ((p→q)→p)→p       ∨i 1,2-5,6-13

2) ((p→⟂)→p)→p⊢ᵢ¬¬p→p

1  ((p→⟂)→p)→p        premise
2  ⌜  ¬¬p             assumption
3  |  (p→⟂)→⟂         ¬ def from 2
4  |  ⌜  p→⟂          assumption
5  |  |  ⟂            →e 3,4
6  |  ⌞  p            ⟂e 5
7  |  (p→⟂)→p         →i 4-6
8  ⌞  p               →e 1,7
----------------------------------
9  ¬¬p→p              →i 2-8
